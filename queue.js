let collection = [];

// Write the queue functions below.

function print() {
    return collection
}

function enqueue(element) {
    collection[collection.length]=element;
    return collection
}

function dequeue() {
    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    }
    collection.length--;
    return collection;
}
  
function front() {
    return collection[0]
}

function size() {
    let i=0;
    while(collection[i]!==undefined){
        i++
    }
    return i
}

function isEmpty() {
    let i=0;
    while(collection[i]!==undefined){
        i++;
    }
    return i===0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};